function Localizar() {
    let mapa = L.map('divMapa', { center: [4.853111,-74.261776], zoom:16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    var polygon = L.polygon([
        [4.850984,-74.26426],
        [4.85278, -74.264566],
        [4.853037, -74.262238],
        [4.853935, -74.262671],
        [4.854184, -74.26182],
        [4.854542, -74.261267],
        [4.85404,-74.261125],
        [4.854115,-74.260409],
        [4.853308,-74.259988],
        [4.85335,-74.259736],
        [4.851811, -74.259532]
    ]).addTo(mapa);

 

var coliseo = L.marker([4.851444, -74.263353], {
title: "Evento1",draggable:true,
opacity: 1
}).bindPopup("<h4>Festival deportivo</h4><li><a href=eventos.html>Evento 1</li></a>")
.addTo(mapa); 

var parque = L.marker([4.852613, -74.260567], {
    title: "Evento2",draggable:true,
    opacity: 1
    }).bindPopup("<h4>Semana cultural</h4><li><a href=eventos.html>Evento 2</li></a>")
    .addTo(mapa); 
}